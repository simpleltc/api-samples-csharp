﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLTC_API_Samples
{
    class ExampleApiClient
    {
        public static IEnumerable<Provider> Providers;

        public const String BaseURL = "https://api-test.simpleltc.com/v1/";
        public const String ApiKey = "";
        public const String Username = "";
        public const String CompanyCode = "";
        public const String Password = "";

        static void Main(string[] args)
        {
            try
            {
                var providers = GetProviders();

                foreach (var provider in providers)
                {
                    Console.WriteLine(provider.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: {0}", ex.Message);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Retrieves a list of Providers from the SimpleLTC API
        /// </summary>
        /// <returns>A list of Providers</returns>
        static IEnumerable<Provider> GetProviders()
        {
            var task = ExecuteApiGet<List<Provider>>("Providers");
            task.Wait();

            return task.Result;
        } 

        /// <summary>
        /// Generic action for performing HTTP GET requests against the API
        /// </summary>
        /// <typeparam name="T">The type of the result to be expected from a successful call</typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        static async Task<T> ExecuteApiGet<T>(String action)
        {
            using (var client = new HttpClient())
            {
                // Set up the initial default headers
                client.BaseAddress = new Uri(BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Set the authentication headers
                client.DefaultRequestHeaders.Add("SLTC-Api-Key", ApiKey);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}@{1}:{2}", Username, CompanyCode, Password))));

                // Make the API call
                var response = await client.GetAsync(action);

                // If the call is successful, return the result.
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<T>();
                }
                
                // If we are not successful, throw an exception.
                var responseAsync = response.Content.ReadAsStringAsync();
                responseAsync.Wait();

                throw new Exception(responseAsync.Result);
            }
        }

    }
}
