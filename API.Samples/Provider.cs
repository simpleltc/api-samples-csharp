﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLTC_API_Samples
{
    [DataContract]
    public class Provider
    {
        [DataMember(Name="id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public String Name { get; set; }

        [DataMember(Name = "state")]
        public String State { get; set; }

        [DataMember(Name = "npi")]
        public String NPI { get; set; }

        [DataMember(Name = "ccn")]
        public String CCN { get; set; }

        [DataMember(Name = "mdsFacilityId")]
        public String MdsFacilityId { get; set; }

        public override string ToString()
        {
            return String.Format("Provider <{0},{1},NPI: {2}, CCN: {3}, State: {4}, MdsFacilityId: {5}>", Id, Name, NPI, CCN, State, MdsFacilityId);
        }
    }
}
